""" Some snippets for pylsl.py library
    """

""" Print stream samples """

from pylsl import StreamInlet, resolve_stream

streams = resolve_stream('type', 'EEG')

# create a new inlet to read from the stream
inlet = StreamInlet(streams[0])

# Show the data sample
while True:
    # get a new sample (you can also omit the timestamp part if you're not
    # interested in it)
    sample, timestamp = inlet.pull_sample()
    print(timestamp, sample)