# This project how-to

## System
Ubuntu 18.04

## Setup

```
### OS (Linux)
LSL (Lab Straming Layer) library needs to be available to the OS.
Take a look at https://github.com/sccn/liblsl
$ git clone https://github.com/sccn/liblsl.git
$ standalone_compilation_linux.sh

### Conda
$ conda update conda
$ conda create --name LSL_Detector
$ conda activate LSL_Detector
$ conda install python=3.8.0

### Python libs
$ pip install pylsl

```

## Useful commands
List Bluetooth MAC addresses:   
```
$ sudo hcitool -i hci0 lescan
```

## Useful links
https://github.com/chkothe/pylsl/blob/master/examples/HandleMetadata.py
