# Cheap and opensource EEG solution - Start processing brain-data with minimal effort, using OpenBCI, Lab Streaming Layer, Python

This short tutorial for beginners explains how to set up an opensource, minimal, cheap 
EEG setup, acquire EEG data into your PC and use them with python, 
using the Lab Streaming Layer protocol.   


## What you need

* BCI hardware: OpenBCI Ganglion (4 channel, open-hardware, EEG solution)   
[more info about OpenBCI Ganglion](https://shop.openbci.com/products/ganglion-board) 

* EEG signal acquisition software for OpenBCI Ganglion   
[OpenBCI GUI software](https://openbci.com/index.php/downloads) 

* Lab Streaming Layer (LSL) library software   
[LSL repository](https://github.com/sccn/labstreaminglayer)   
For a short tutorial on installing LSL library on Linux:
[environment-how-to](environment-how-to.md)  


## How it works
* Switch on the OpenBCI Ganglion Board   
* Run the OpenBCI_GUI software:   
```$ sudo ./OpenBCI_GUI ```   
* From the OpenBCI_GUI, connect to the Ganglion board and start acquiring the signal.   
[OpenBCI_GUI tutorial](https://docs.openbci.com/docs/06Software/01-OpenBCISoftware/GUIDocs)   
* From the OpenBCI_GUI, start the LSL streaming (Note that LSL library needs to be installed on your PC)
* Run the main script that comes with this repository   
```$ python main.py ```  
You can take a look to this short tutorial for the python setup: 
[environment-how-to](environment-how-to.md)
* You can edit the script ```main.py``` for brain signal manipulation.  
You can take a look to [this](snippets.py) snippet that shows how to read brain signal samples inside the provided ```main.py``` script.
