"""Example program to show how to read a multi-channel time series from LSL."""
import sys
import datetime
from pylsl import StreamInlet, resolve_stream

def main(argv):
    # first resolve an EEG stream on the lab network
    print("looking for an EEG stream...")
    streams = resolve_stream('type', 'EEG')
    # create a new inlet to read from the stream
    inlet = StreamInlet(streams[0])
    # List LSL streams
    for i in range(len(streams)):
        print("EEG stream n. " + str(i) + ':')
        inlet = StreamInlet(streams[i])
        streamInfo = inlet.info()
        streamInfoName = streamInfo.name()
        
        print(" Name: " + streamInfoName)
        print(" Type: " + streamInfo.type())
        print(" Channel_count: " + str(streamInfo.channel_count()))
        print(" Sampling rate: " + str(streamInfo.nominal_srate()))
        print(" Channel format: " + str(streamInfo.channel_format()))
        print(" Source id: " + streamInfo.source_id())
        print(" LSL version: " + str(streamInfo.version()))
        print(" Created at: " + str(datetime.datetime.fromtimestamp(streamInfo.created_at())))
    
if __name__ == "__main__":
    main(sys.argv[1:])