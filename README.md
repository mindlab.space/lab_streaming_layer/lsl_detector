# LSL Detector

![LSL Detector screenshot](docs/lsl_detector.png "LSL Detector screenshot")


A simple python script that list the LSL streams he found 
over the network.   
Read more about LSL:
https://labstreaminglayer.readthedocs.io/

## Pre-requisite (How-to install)
How-to setup the environment: [environment-how-to](docs/environment-how-to.md)  
The instructions will guide you through the installation of the prerequisites and dependencies for the Lab Streaming Layer protocol to work.

## Usage
```
$ python main.py
```

## Extra
A short tutorial for a cheap and opensource BCI EEG setup can be found here:  
[cheap-and-opensource-EEG-solution-lab-streaming-layer](docs/cheap-and-opensource-EEG-solution-lab-streaming-layer.md)